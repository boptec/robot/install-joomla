*** Settings ***
Resource        resources/ComumptBR.robot
Resource        resources/PO/InstallptBR.robot
Resource        resources/PO/LoginBackendptBR.robot
Resource        resources/PO/LoginFrontendptBR.robot
Resource        resources/PO/Upgrade310ptBR.robot
Test Setup      Abrir Navegador
Test Teardown   Fechar Navegador

*** Variables ***
${HOSTDB}  j3db
${NOMEBANCO}  bancoj3

*** Test Cases ***
Caso de Teste 04: Atualização do Joomla 3.10
    Abrir Página Administrator do Joomla
    Login no Administrator do Joomla com "usuario1" e senha "senha1"
    Atualização do Joomla
    Logout do Administrator

Caso de Teste 05: Pre-update check para Joomla 4
    Abrir Página Administrator do Joomla
    Login no Administrator do Joomla com "usuario1" e senha "senha1"
    Executa pre-update check
    Logout do Administrator
