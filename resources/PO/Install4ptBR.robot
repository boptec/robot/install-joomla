*** Settings ***
Documentation   Recursos necessários para testar a instalação do Joomla
Library         SeleniumLibrary

*** Variables ***
${URL_INSTALL}  http://j4/installation/index.php
${BROWSER}      chrome
# Para executar o chrome no docker linux, precisa passar os argumentos abaixo pois o chrome não aceita,
# por padrão, executar como root, e com estes parametros ele aceita
# Referencia: https://gitlab.com/myrobotprojects/robotavancado/-/blob/master/resources/Resource.robot
${OPTIONS}      add_argument("--disable-dev-shm-usage"); add_argument("--headless"); add_argument("--no-sandbox"); add_argument("--window-size=1920,1080")


*** Keywords ***
Abrir Pagina de Instalação
    Go To           ${URL_INSTALL}
    Capture Page Screenshot

#### Passo a passo
Selecionar "${LANG}" na instalação
    Wait Until Element Is Visible   id=jform_language  timeout=15
    Capture Page Screenshot
    # Não esta disponível ainda pt-BR
    Select From List By Value       id=jform_language  pt-PT
    Capture Page Screenshot

Digitar o nome do site "${NOME_SITE}" no campo do nome do site
    Input Text     id=jform_site_name  ${NOME_SITE}
    Click Element  id=step1

Digitar o nome real "${ADMIN_REAL}" no campo nome do Super utilizador
    Wait Until Element Is Visible   id=jform_admin_user  timeout=15
    Input Text                      id=jform_admin_user  ${ADMIN_REAL}

Digitar o nome do utilizador "${USER_ADMIN}" no campo de nome de utilizador
    Input Text  id=jform_admin_username  ${USER_ADMIN}

Digitar a senha "${USER_PASS}" nos campos de senha
    Input Text  id=jform_admin_password  ${USER_PASS}

Digitar o email "${EMAIL_ADMIN}" no campo do e-mail do administrador
    Input Text  id=jform_admin_email  ${EMAIL_ADMIN}

Clicar no botão próximo da página Configuração
    Capture Page Screenshot
    Execute JavaScript              window.scrollTo(0, document.body.scrollHeight)
    Capture Page Screenshot
    Click Element  id=step2

Digitar o nome do servidor "${HOSTDB}" no campo de nome do servidor
    Wait Until Element Is Visible  id=jform_db_host
    Input Text  id=jform_db_host  ${HOSTDB}

Digitar o nome do utilizador "${DB_USER}" no campo de nome do usuario do DB
    Wait Until Element Is Visible  id=jform_db_user
    Input Text  id=jform_db_user  ${DB_USER}

Digitar a senha "${DB_PASS}" no campo senha do banco de dados
    Wait Until Element Is Visible  id=jform_db_pass
    Input Text  id=jform_db_pass  ${DB_PASS}

Digitar o nome do banco "${DB_NAME}" no campo nome do banco
    Wait Until Element Is Visible  id=jform_db_name
    Input Text  id=jform_db_name  ${DB_NAME}

Clicar no botão próximo da página Banco de dados
    Capture Page Screenshot
    Execute JavaScript              window.scrollTo(0, document.body.scrollHeight)
    Capture Page Screenshot
    Click Element  id=setupButton

Instalar idioma pt-br
    # Aguarda a mensagem de instalado com sucesso aparecer
    Wait Until Element Is Visible   id=installAddFeatures  timeout=30
    Capture Page Screenshot
    # Clica no botão de instalar linguagens
    Click Element                   id=installAddFeatures
    Capture Page Screenshot
    Press Keys                      None  PAGE_DOWN
    Capture Page Screenshot
    #Execute Javascript   WebElement element = driver.findElement(By.id("cb22"));((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element); Thread.sleep(500); 
    #Capture Page Screenshot
    #Scroll Element Into View        xpath=//*/label[@for='cb22']
    Execute JavaScript              window.scrollTo(0, document.body.scrollHeight)
    Capture Page Screenshot
    # Ainda não foi ativado o Português do Brasil pois ainda não foi liberado pela equipe de tradução
    #Wait Until Element Is Visible   xpath=//*/label[contains(text(),'Portuguese Brazil')]
    # Aguarda o checkbox  de pt-PT
    Wait Until Element Is Visible   xpath=//*/label[contains(text(),'Portuguese, Brazil')]
    # Confirma que o elemento é Português de Portugal
    Element Should Contain          xpath=//*/label[@for='cb23']  Portuguese, Brazil
    
    # Marca o checkbox
    Select Checkbox                 id=cb23
    # Instala a linguagem
    Click Element                   id=installLanguagesButton
    # Aguarda a página seguinte de seleção da linguagem padrão aparecer
    Wait Until Element Is Visible   id=defaultLanguage   30
    # Seleciona o radio-button do Português do Brasil como padrão para o admin
    Select Radio Button             administratorlang  pt-BR
    # Seleciona o radio-button do Português do Brasil como padrão para o frontend
    Select Radio Button             frontendlang       pt-BR
    Click Element                   id=defaultLanguagesButton
    Capture Page Screenshot
    Execute JavaScript              window.scrollTo(0, document.body.scrollHeight)
    Capture Page Screenshot
    # Clica no botão Concluir e abrir administração
    Click Element                   xpath=//*[@id="installRecommended"]/div/div/button[2]


Remove pasta de instalação
    Wait Until Element Is Visible   xpath=//*[@id="system-message-container"]/div/div[contains(text(),'Joomla definiu pt-BR como o idioma padrão do Administrador.')]  timeout=30
    Click Element                   xpath=//*[@id="adminForm"]/div[3]/input[@name='instDefault']
    Wait Until Element Is Visible   xpath=//*[@id="adminForm"]/div[3]/input[@value='Pasta Installation removida.']
    Capture Page Screenshot
