*** Keywords ***
Atualização do Joomla
    Wait Until Element Is Visible   xpath=//*[@id="system-message-container"]/div[1]/button     timeout=30
    # Verifica se a versão disponível para atualização é a 3.10.0
    Wait Until Element Is Visible   xpath=//*[@id="system-message-container"]/div[1]/span       timeout=30
    Element Should Contain          xpath=//*[@id="system-message-container"]/div[1]/span       3.10.0
    Capture Page Screenshot
    Click Element                   xpath=//*[@id="system-message-container"]/div[1]/button
    # Aguarda quadro de resumo do upgrade
    Wait Until Element Is Visible   xpath=//*[@id="adminForm"]/fieldset/table/tbody/tr[1]/td[contains(text(), '3.9.28')]
    Scroll Element Into View        xpath=//*[@id="adminForm"]/fieldset/table/tfoot/tr/td[2]/button
    Execute JavaScript              window.scrollTo(0, document.body.scrollHeight)
    Capture Page Screenshot
    Click Element                   xpath=//*/button[contains(text(), 'Instalar a atualização')]
    #Click Element                   xpath=//*[@id="adminForm"]/fieldset/table/tfoot/tr/td[2]/button
    # Aguarda o fim da atualização
    Wait Until Element Is Visible   xpath=//*/p[contains(text(), 'Seu site foi atualizado com sucesso.')]   timeout=60
    Capture Page Screenshot

Executa pre-update check
    # Clica no menu componentes
    Click Element                   xpath=//*[@id="menu"]/li[5]/a
    # Clica no sub-menu Atualização Joomla
    Click Element                   xpath=//*[@id="menu"]/li[5]/ul/li[2]/a
    Wait Until Element Is Visible   xpath=//*[@id="toolbar-loop"]/button    timeout=30
    Element Should Contain          xpath=//*[@id="toolbar-loop"]/button     Verificar
    Capture Page Screenshot
    Click Element                   xpath=//*[@id="toolbar-loop"]/button
    Wait Until Element Is Visible   xpath=//*[@id="pre-update-check"]/div[1]/fieldset[1]/legend/h3/div  timeout=30
    Execute JavaScript              window.scrollTo(0, document.body.scrollHeight)
    Capture Page Screenshot
