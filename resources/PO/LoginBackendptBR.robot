*** Settings ***
Documentation   Recursos necessários para testar a instalação do Joomla
Library         SeleniumLibrary

*** Variables ***
${URL_ADMINISTRATOR}    http://joomla/administrator/index.php
# Para executar o chrome no docker linux, precisa passar os argumentos abaixo pois o chrome não aceita,
# por padrão, executar como root, e com estes parametros ele aceita
# Referencia: https://gitlab.com/myrobotprojects/robotavancado/-/blob/master/resources/Resource.robot
${OPTIONS}              add_argument("--disable-dev-shm-usage"); add_argument("--headless"); add_argument("--no-sandbox")

*** Keywords ***
Abrir Página Administrator do Joomla
    Go To           ${URL_ADMINISTRATOR}
    Capture Page Screenshot


Login no Administrator do Joomla com "${USER_ADMIN}" e senha "${USER_PASS}"
    Wait Until Element Is Visible   id=mod-login-username
    Capture Page Screenshot
    Input Text                      id=mod-login-username  ${USER_ADMIN}
    Input Text                      id=mod-login-password  ${USER_PASS}
    Capture Page Screenshot
    Click Element                   xpath=//*[@id="form-login"]/fieldset/div[4]/div/div/button
    Wait Until Element Is Visible   xpath=/html/body/header/div[2]/h1[@class='page-title']
    Capture Page Screenshot

Logout do Administrator
    Click Element    xpath=//*/nav/div/div/div/ul[3]/li/a
    Click Element    xpath=//*/nav/div/div/div/ul[3]/li/ul/li[5]/a[contains(text(),'Sair')]
    Capture Page Screenshot

# Clica no botão de enviar as estatísticas sempre (pra não atrapalhar os outros testes)
Enviar estatisticas sempre
    Wait Until Element Is Visible   xpath=//*/a[contains(text(), 'Sempre')]     timeout=30
    Click Element                   xpath=//*/a[contains(text(), 'Sempre')]
