*** Settings ***
Documentation   Recursos necessários para testar a instalação do Joomla
Library         SeleniumLibrary

*** Variables ***
${URL_INSTALL}  http://joomla/installation/index.php
${BROWSER}      chrome
# Para executar o chrome no docker linux, precisa passar os argumentos abaixo pois o chrome não aceita,
# por padrão, executar como root, e com estes parametros ele aceita
# Referencia: https://gitlab.com/myrobotprojects/robotavancado/-/blob/master/resources/Resource.robot
${OPTIONS}      add_argument("--disable-dev-shm-usage"); add_argument("--headless"); add_argument("--no-sandbox"); add_argument("--window-size=1920,1080")


*** Keywords ***
Abrir Pagina de Instalação
    Go To           ${URL_INSTALL}
    Capture Page Screenshot

#### Passo a passo
Selecionar "${LANG}" na instalação
    Wait Until Element Is Visible   xpath=//*[@id="jform_language_chzn"]  timeout=15
    Capture Page Screenshot
    Click Element                   xpath=//*[@id="jform_language_chzn"]
    Input Text                      xpath=//*[@id="jform_language_chzn"]/div/div/input  ${LANG}
    Click Element                   xpath=//*[@id="jform_language_chzn"]/div/ul/li
    Capture Page Screenshot

Digitar o nome do site "${NOME_SITE}" no campo do nome do site
    Input Text  id=jform_site_name  ${NOME_SITE}

Digitar o email "${EMAIL_ADMIN}" no campo do e-mail do administrador
    Input Text  id=jform_admin_email  ${EMAIL_ADMIN}

Digitar o nome do usuario "${USER_ADMIN}" no campo de nome do administrador
    Input Text  id=jform_admin_user  ${USER_ADMIN}

Digitar a senha "${USER_PASS}" nos campos de senha
    Input Text  id=jform_admin_password  ${USER_PASS}
    Input Text  id=jform_admin_password2  ${USER_PASS}

Clicar no botão próximo da página Configuração
    Capture Page Screenshot
    Click Element  xpath=//*[@id="container-installation"]/div/div/a[@rel='next']

Digitar o nome do servidor "${HOSTDB}" no campo de nome do servidor
    Wait Until Element Is Visible  id=jform_db_host
    Input Text  id=jform_db_host  ${HOSTDB}

Digitar o nome do usuario "${DB_USER}" no campo de nome do usuario do DB
    Wait Until Element Is Visible  id=jform_db_user
    Input Text  id=jform_db_user  ${DB_USER}

Digitar a senha "${DB_PASS}" no campo senha do banco de dados
    Wait Until Element Is Visible  id=jform_db_pass
    Input Text  id=jform_db_pass  ${DB_PASS}

Digitar o nome do banco "${DB_NAME}" no campo nome do banco
    Wait Until Element Is Visible  id=jform_db_name
    Input Text  id=jform_db_name  ${DB_NAME}

Clicar no botão próximo da página Banco de dados
    Capture Page Screenshot
    Click Element  xpath=//*[@id="adminForm"]/div[1]/div/a[2][@rel='next']

Clicar no botão instalar da página Finalização
    Wait Until Element Is Visible  xpath=//*[@id="adminForm"]/div[6]/div[1]/table/tbody/tr[11]/td[2]/span  timeout=120
    Capture Page Screenshot
    Click Element                  xpath=//*[@id="adminForm"]/div[1]/div/a[2][@rel='next']

Instalar idioma pt-br
    # Aguarda a mensagem de instalado com sucesso aparecer
    Wait Until Element Is Visible   xpath=//*[@id="adminForm"]/div[@class='alert alert-success']/h3  timeout=30
    Capture Page Screenshot
    # Clica no botão de instalar linguagens
    Click Element                   xpath=//*[@id="instLangs"]
    # Aguarda o checkbox  de pt-br
    Wait Until Element Is Visible   xpath=//*/label[contains(text(),'Portuguese Brazil')]
    # Confirma que o elemento é Português do Brazil
    Element Should Contain          xpath=//*/label[@for='cb56']  Portuguese Brazil
    # Marca o checkbox
    Select Checkbox                 id=cb56
    # Instala a linguagem
    Click Element                   xpath=//*[@id="adminForm"]/div[3]/div/div/a[@rel='next']
    # Aguarda a página seguinte de seleção da linguagem padrão aparecer
    Wait Until Element Is Visible   xpath=//*[@id="jform_activateMultilanguage"]  15
    # Seleciona o radio-button do Português do Brasil como padrão para o admin
    Select Radio Button             administratorlang  pt-BR
    # Seleciona o radio-button do Português do Brasil como padrão para o frontend
    Select Radio Button             frontendlang       pt-BR
    Click Element                   xpath=//*[@id="adminForm"]/div[1]/div/a[@rel='next']
    Capture Page Screenshot

Remove pasta de instalação
    Wait Until Element Is Visible   xpath=//*[@id="system-message-container"]/div/div[contains(text(),'Joomla definiu pt-BR como o idioma padrão do Administrador.')]  timeout=30
    Click Element                   xpath=//*[@id="adminForm"]/div[3]/input[@name='instDefault']
    Wait Until Element Is Visible   xpath=//*[@id="adminForm"]/div[3]/input[@value='Pasta Installation removida.']
    Capture Page Screenshot

