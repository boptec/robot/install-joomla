*** Settings ***
Documentation   Recursos necessários para testar a instalação do Joomla
Library         SeleniumLibrary

*** Variables ***
${URL_ADMINISTRATOR}    http://joomla/administrator/index.php
# Para executar o chrome no docker linux, precisa passar os argumentos abaixo pois o chrome não aceita,
# por padrão, executar como root, e com estes parametros ele aceita
# Referencia: https://gitlab.com/myrobotprojects/robotavancado/-/blob/master/resources/Resource.robot
${OPTIONS}              add_argument("--disable-dev-shm-usage"); add_argument("--headless"); add_argument("--no-sandbox")

*** Keywords ***
Abrir Página Administrator do Joomla
    Go To           ${URL_ADMINISTRATOR}
    Capture Page Screenshot


Login no Administrator do Joomla com "${USER_ADMIN}" e senha "${USER_PASS}"
    Wait Until Element Is Visible   id=mod-login-username
    Capture Page Screenshot
    Input Text                      id=mod-login-username  ${USER_ADMIN}
    Input Text                      id=mod-login-password  ${USER_PASS}
    Capture Page Screenshot
    Click Element                   id=btn-login-submit
    Wait Until Element Is Visible   xpath=//*[@class="page-title"]
    Element Should Contain          xpath=//*[@id="header"]/div[2]/div[1]/div[2]/div/h1    Painel Principal
    Capture Page Screenshot

Logout do Administrator
    Capture Page Screenshot
    Click Element    xpath=//*[@id="header"]/div[2]/div[2]/div[4]/div/button
    Capture Page Screenshot
    Click Element    xpath=//*[@id="header"]/div[2]/div[2]/div[4]/div/div/a[3]
    Capture Page Screenshot

# Clica no botão de enviar as estatísticas sempre (pra não atrapalhar os outros testes)
Enviar estatisticas sempre
    Wait Until Element Is Visible   xpath=//*/button[contains(text(), 'Sempre')]     timeout=30
    Click Element                   xpath=//*/button[contains(text(), 'Sempre')]
