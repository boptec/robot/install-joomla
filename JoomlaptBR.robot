*** Settings ***
Resource        resources/ComumptBR.robot
Resource        resources/PO/InstallptBR.robot
Resource        resources/PO/LoginBackendptBR.robot
Resource        resources/PO/LoginFrontendptBR.robot
Resource        resources/PO/Upgrade310ptBR.robot
Test Setup      Abrir Navegador
Test Teardown   Fechar Navegador

*** Variables ***
${HOSTDB}  j3db
${NOMEBANCO}  bancoj3

*** Test Cases ***
Caso de Teste 01: Instalação do Joomla
    Abrir Pagina de Instalação
    Selecionar "pt-BR" na instalação
    Digitar o nome do site "Teste Joomla!" no campo do nome do site
    Digitar o email "teste@bop.tec.br" no campo do e-mail do administrador
    Digitar o nome do usuario "usuario1" no campo de nome do administrador
    Digitar a senha "senha1" nos campos de senha
    Clicar no botão próximo da página Configuração
    Digitar o nome do servidor "${HOSTDB}" no campo de nome do servidor
    Digitar o nome do usuario "root" no campo de nome do usuario do DB
    Digitar a senha "example" no campo senha do banco de dados
    Digitar o nome do banco "${NOMEBANCO}" no campo nome do banco
    Clicar no botão próximo da página Banco de dados
    Clicar no botão instalar da página Finalização
    Instalar idioma pt-br
    Remove pasta de instalação

Caso de Teste 02: Login no Administrator
    Abrir Página Administrator do Joomla
    Login no Administrator do Joomla com "usuario1" e senha "senha1"
    Enviar estatisticas sempre
    Logout do Administrator

Caso de Teste 03: Login no Frontend
    Abrir Página Frontend do Joomla
    Login no Frontend do Joomla com "usuario1" e "senha1"
    Logout do Frontend
